use std::collections::HashMap;
use std::rc::Rc;
#[derive(PartialEq, Debug, Hash)]
pub struct Node<T> {
    value: T,
    connections_in: HashMap<usize, Rc<Line<T>>>,
    connections_out: HashMap<usize, Rc<Line<T>>>,
}
impl<T> Node<T> {
    pub fn from(v: T) -> Node<T> {
        Node {
            value: v,
            connections_in: HashMap::new(),
            connections_out: HashMap::new(),
        }
    }
    pub fn in_count(&self) -> usize {
        self.connections_in.len()
    }
    pub fn out_count(&self) -> usize {
        self.connections_out.len()
    }
    pub fn connection_count(&self) -> usize {
        self.get_in_count() + self.get_out_count()
    }
    pub fn insert_in(&mut self, v: Rc<Line<T>>) {
        self.connections_in.insert(self.connections_in.len(), v);
    }
    pub fn delete_in(&mut self, v: Rc<Line<T>>) {
        for (key, conn) in &self.connections_in {
            if *conn == v {
                self.connections_in.remove(key);
            }
        }
    }
    pub fn delete_key_in(&mut self, key: &usize) {
        if self.connections_in.contains_key(key) {
            self.connections_in.remove(key)
        }
    }
    pub fn insert_out(&mut self, v: Rc<Line<T>>) {
        self.connections_out.insert(self.connections_out.len(), v);
    }
    pub fn delete_out(&mut self, v: Rc<Line<T>>) {
        for (key, conn) in &self.connections_out {
            if *conn == v {
                self.connections_out.remove(key);
            }
        }
    }
    pub fn delete_key_out(&mut self, key: &usize) {
        if self.connections_out.contains_key(key) {
            self.connections_out.remove(key)
        }
    }
}
#[derive(PartialEq, Debug, Hash)]
pub struct Line<T> {
    from: Option<Rc<Node<T>>>,
    to: Option<Rc<Node<T>>>,
}
impl<T> Line<T> {
    pub fn new() -> Line<T> {
        Line {
            from: None,
            to: None,
        }
    }
    pub fn from_to(f: Option<Rc<Node<T>>>, t: Option<Rc<Node<T>>>) -> Line<T> {
        Line {
            from: f,
            to: t,
        }
    }
}
#[derive(PartialEq, Debug, Hash)]
pub struct Graph<T> {
    top: Option<Rc<Node<T>>>,
    end: Option<Rc<Node<T>>>,
    node_count: usize,
    line_count: usize,
}
impl<T> Graph<T> {
    pub fn new() -> Graph<T> {
        Graph {
            top: None,
            end: None,
            node_count: 0,
            line_count: 0,
        }
    }
    pub fn get_top(&self) -> Option<Rc<Node<T>>> {
        self.top.clone()
    }
    pub fn set_top(&mut self, new_top: Rc<Node<T>>) {
        self.top = Some(new_top)
    }
    pub fn get_end(&self) -> Option<Rc<Node<T>>> {
        self.end.clone()
    }
    pub fn set_end(&mut self, new_end: Rc<Node<T>>) {
        self.end = Some(new_end)
    }
    pub fn add_to_end(&mut self, v: T) {
        if self.top.is_none() && self.end.is_none() {
            self.set_top(Rc::new(Node::from(v)));
            self.set_end(self.get_top().unwrap());
            return;
        }
        let temp = Some(Rc::new(Node::from(v)));
    }
}
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result: Graph<i32> = Graph::new();
        assert_eq!(result.get_top(), None);
    }
}
